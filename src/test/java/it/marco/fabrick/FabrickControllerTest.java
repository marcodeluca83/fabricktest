package it.marco.fabrick;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.marco.fabrick.controller.FabrickController;
import it.marco.fabrick.dto.InputBonificoDto;
import it.marco.fabrick.dto.InputTransazioniDto;
import it.marco.fabrick.json.BonificoJson;
import it.marco.fabrick.json.SaldoJson;
import it.marco.fabrick.json.TransazioniJson;
import it.marco.fabrick.utils.Constants;

@RunWith(SpringRunner.class)
@WebMvcTest(FabrickController.class)
@WebAppConfiguration
@ContextConfiguration(classes = PersistenceJpaConfig.class)
public class FabrickControllerTest {
	
	private static final Logger logger = LoggerFactory.getLogger(FabrickControllerTest.class);
	
	private ObjectMapper objectMapper = new ObjectMapper();
	private HttpHeaders httpHeaders = new HttpHeaders();

    @Autowired
    private MockMvc mvc;
    
    @BeforeEach 
    public void init() {
    	httpHeaders.add(Constants.AUTH_SCHEMA, Constants.S2S);
    	httpHeaders.add(Constants.API_KEY, Constants.KEY);
    }
    
    @Test
    public void t1() throws Exception {
     
    	logger.info("testing /saldo");
    	MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
    	params.add("accountId", Constants.ACCOUNT_ID.toString());
    	
        ResultActions resultActions = mvc.perform(get("/saldo")
        		.headers(httpHeaders)
        		.params(params)
        		.contentType(MediaType.APPLICATION_JSON))
        		.andExpect(status().isOk());
        
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();

        SaldoJson response = objectMapper.readValue(contentAsString, SaldoJson.class);

    }
    
    @Test
    public void t2() throws Exception {
    	
    	logger.info("testing /transazioni");
    	InputTransazioniDto input = new InputTransazioniDto();
    	input.setAccountId(Constants.ACCOUNT_ID);
    	input.setFromAccountingDate("2020-01-01");
    	input.setToAccountingDate("2020-01-30");
    	
        ResultActions resultActions = mvc.perform(get("/transazioni")
        		.headers(httpHeaders)
        		.content(objectMapper.writeValueAsString(input))
        		.contentType(MediaType.APPLICATION_JSON))
        		.andExpect(status().isOk());
        
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();

        TransazioniJson response = objectMapper.readValue(contentAsString, TransazioniJson.class);
    }
    
    @Test
    public void t3() throws Exception {
    	
    	logger.info("testing /bonifico");
    	InputBonificoDto input = new InputBonificoDto();
    	input.setAccountId(Constants.ACCOUNT_ID);
    	input.setAmount("10.55");
    	input.setCurrency("EUR");
    	input.setDescription("descrizione test");
    	input.setReceiverName("Mario Rossi");
    	input.setExecutionDate("01/01/2020");
    	
        ResultActions resultActions = mvc.perform(post("/bonifico")
        		.headers(httpHeaders)
        		.content(objectMapper.writeValueAsString(input))
        		.contentType(MediaType.APPLICATION_JSON))
        		.andExpect(status().isOk());
        
        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();

        BonificoJson response = objectMapper.readValue(contentAsString, BonificoJson.class);
    }
	
}