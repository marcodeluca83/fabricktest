package it.marco.fabrick.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import it.marco.fabrick.dto.InputBonificoDto;
import it.marco.fabrick.dto.InputTransazioniDto;
import it.marco.fabrick.dto.OutputBonificoDto;
import it.marco.fabrick.json.DettaglioBonificoJson;
import it.marco.fabrick.json.DettaglioSaldoJson;
import it.marco.fabrick.json.DettaglioTransazioniJson;
import it.marco.fabrick.service.FabrickService;
import it.marco.fabrick.validator.CustomInputValidator;

@RestController
public class FabrickController {
	
	private static final Logger logger = LoggerFactory.getLogger(FabrickController.class);
	
	@Autowired @Qualifier("fabrickService")
	public FabrickService fabrickService;

	@GetMapping("/saldo")
	public DettaglioSaldoJson getSaldo(@RequestParam Long accountId) {
		
		logger.info("getSaldo");
		try {
			return fabrickService.letturaSaldoOperation(accountId);
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}
	
	@GetMapping("/transazioni")
	public DettaglioTransazioniJson getTransazioni(@RequestBody InputTransazioniDto input) {
		
		logger.info("getTransazioni");
		try {
			CustomInputValidator.TransazioniValidator(input);
			return fabrickService.letturaTransazioniOperation(input);
		} catch (Exception e){
			logger.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}
	
	@PostMapping("/bonifico")
	public OutputBonificoDto postBonifico(@RequestBody InputBonificoDto input) {
		
		logger.info("postBonifico");
		try {
			CustomInputValidator.BonificoValidator(input);
			DettaglioBonificoJson outputJson = fabrickService.bonificoOperation(input);
			OutputBonificoDto output = new OutputBonificoDto();
			//in caso di esito positivo, si settano i valori dal json al dto
			return output;
		} catch (Exception e){
			logger.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage(), e);
		}
	}
}
