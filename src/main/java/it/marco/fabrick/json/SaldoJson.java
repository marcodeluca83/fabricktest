package it.marco.fabrick.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SaldoJson {
	
	private String status;
	private List<ErrorJson> error;
	private DettaglioSaldoJson payload;

	public SaldoJson() {
		
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public DettaglioSaldoJson getPayload() {
		return payload;
	}

	public void setPayload(DettaglioSaldoJson payload) {
		this.payload = payload;
	}

	public List<ErrorJson> getError() {
		return error;
	}

	public void setError(List<ErrorJson> error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "SaldoJson [status=" + status + ", error=" + error + ", payload=" + payload + "]";
	}
	
}
