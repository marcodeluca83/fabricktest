package it.marco.fabrick.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TransazioniJson {
	
	private String status;
	private List<ErrorJson> error;
	private DettaglioTransazioniJson payload;

	public TransazioniJson() {
		
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<ErrorJson> getError() {
		return error;
	}

	public void setError(List<ErrorJson> error) {
		this.error = error;
	}

	public DettaglioTransazioniJson getPayload() {
		return payload;
	}

	public void setPayload(DettaglioTransazioniJson payload) {
		this.payload = payload;
	}

	@Override
	public String toString() {
		return "TransazioniJson [status=" + status + ", error=" + error + ", payload=" + payload + "]";
	}

}
