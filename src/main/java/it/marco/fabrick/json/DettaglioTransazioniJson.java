package it.marco.fabrick.json;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DettaglioTransazioniJson {
	
	private List<DettaglioTransazioneJson> list;

	public DettaglioTransazioniJson() {
		
	}

	public List<DettaglioTransazioneJson> getList() {
		return list;
	}

	public void setList(List<DettaglioTransazioneJson> list) {
		this.list = list;
	}

	@Override
	public String toString() {
		return "DettaglioTransazioniJson [list=" + list + "]";
	}

}
