package it.marco.fabrick.json;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class DettaglioBonificoJson {
	
	private String code;
	private String description;
	
	public DettaglioBonificoJson() {
		
	}
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "DettaglioBonificoJson [code=" + code + ", description=" + description + "]";
	}
	
}
