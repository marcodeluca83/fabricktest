package it.marco.fabrick.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import it.marco.fabrick.dto.InputBonificoDto;
import it.marco.fabrick.dto.InputTransazioniDto;

public class CustomInputValidator {
	
	//si ipotizza che il punto sia divisorio fra cifre intere e decimali
	private static Pattern patternNumeric = Pattern.compile("-?\\d+(\\.\\d+)?");
	private static SimpleDateFormat formatDateBonifico = new SimpleDateFormat("dd/MM/yyyy");
	private static SimpleDateFormat formatDateTransazioni = new SimpleDateFormat("yyyy-MM-dd");

	public static void TransazioniValidator(InputTransazioniDto input) throws Exception{
		
		if (!isValidDate(input.getFromAccountingDate(), formatDateTransazioni)) {
			throw new Exception("date from non valida");
		} else if (!isValidDate(input.getToAccountingDate(), formatDateTransazioni)) {
			throw new Exception("date to non valida");
		} 
		if(!correctDates(input.getFromAccountingDate(), input.getToAccountingDate(), formatDateTransazioni)) {
			throw new Exception("date from non precedente a date to");
		}
		
	}
	
	public static void BonificoValidator(InputBonificoDto input) throws Exception{
		//non sono stati specificati gli input mandatori, ipotizzo che siano tutti non nulli
		
		if(!isNumeric(input.getAmount())) {
			throw new Exception("amount non numerico");
		} else if (!isValidDate(input.getExecutionDate(), formatDateBonifico)) {
			throw new Exception("execution date non valida");
		} 
		//sulla currency bisognerebbe avere tutte quelle gestite dal sistema (euro, dollaro, sterlina ...)
		
	}
	
	private static boolean isNumeric(String strNum) {
	    if (strNum == null) {
	        return false; 
	    }
	    return patternNumeric.matcher(strNum).matches();
	}
	
	private static boolean isValidDate(String inDate, SimpleDateFormat format) {
        try {
        	format.parse(inDate.trim());
        } catch (ParseException pe) {
            return false;
        }
        return true;
    }
	
	private static boolean correctDates(String from, String to, SimpleDateFormat format) {
		try {
        	Date dateFrom = format.parse(from);
        	Date dateTo = format.parse(to);
        	if(dateFrom.compareTo(dateTo) > 0) 
        		return false;
        } catch (ParseException pe) {
            return false;
        }
        return true;
	}
}
