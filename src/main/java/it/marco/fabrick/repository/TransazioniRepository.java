package it.marco.fabrick.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.marco.fabrick.entity.TransazioniEntity;

@Repository
public interface TransazioniRepository extends CrudRepository<TransazioniEntity, Long>{

}
