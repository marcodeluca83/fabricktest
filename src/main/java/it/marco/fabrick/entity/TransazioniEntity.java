package it.marco.fabrick.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Transazioni")
public class TransazioniEntity {

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Long id;
	
	@Column(name = "accountId")
	private Long accountId;
	
	@Column(name = "iban")
	private String iban;
	
	@Column(name = "abiCode")
	private String abiCode;
	
	@Column(name = "cabCode")
	private String cabCode;
	
	@Column(name = "countryCode")
	private String countryCode;
	
	@Column(name = "internationalCin")
	private String internationalCin;
	
	@Column(name = "nationalCin")
	private String nationalCin;
	
	@Column(name = "account")
	private String account;
	
	@Column(name = "alias")
	private String alias;
	
	@Column(name = "productName")
	private String productName;
	
	@Column(name = "holderName")
	private String holderName;
	
	@Column(name = "activatedDate")
	private Date activatedDate;
	
	@Column(name = "currency")
	private String currency;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getAccountId() {
		return accountId;
	}
	public void setAccountId(Long accountId) {
		this.accountId = accountId;
	}
	public String getIban() {
		return iban;
	}
	public void setIban(String iban) {
		this.iban = iban;
	}
	public String getAbiCode() {
		return abiCode;
	}
	public void setAbiCode(String abiCode) {
		this.abiCode = abiCode;
	}
	public String getCabCode() {
		return cabCode;
	}
	public void setCabCode(String cabCode) {
		this.cabCode = cabCode;
	}
	public String getCountryCode() {
		return countryCode;
	}
	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}
	public String getInternationalCin() {
		return internationalCin;
	}
	public void setInternationalCin(String internationalCin) {
		this.internationalCin = internationalCin;
	}
	public String getNationalCin() {
		return nationalCin;
	}
	public void setNationalCin(String nationalCin) {
		this.nationalCin = nationalCin;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getAlias() {
		return alias;
	}
	public void setAlias(String alias) {
		this.alias = alias;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getHolderName() {
		return holderName;
	}
	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}
	public Date getActivatedDate() {
		return activatedDate;
	}
	public void setActivatedDate(Date activatedDate) {
		this.activatedDate = activatedDate;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
}
