package it.marco.fabrick;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import it.marco.fabrick.utils.Constants;

public class CustomClientHttpRequestInterceptor implements ClientHttpRequestInterceptor {
	
    private static Logger logger = LoggerFactory.getLogger(CustomClientHttpRequestInterceptor.class);
 
    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, 
    		ClientHttpRequestExecution execution) throws IOException {
  
    	request.getHeaders().add(Constants.AUTH_SCHEMA, Constants.S2S);
    	request.getHeaders().add(Constants.API_KEY, Constants.KEY);
        logRequestDetails(request);
        return execution.execute(request, body);
    }
    
    private void logRequestDetails(HttpRequest request) {
        logger.info("Headers: {}", request.getHeaders());
        logger.info("Request Method: {}", request.getMethod());
        logger.info("Request URI: {}", request.getURI());
    }
}
