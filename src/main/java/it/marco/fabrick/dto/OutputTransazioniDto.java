package it.marco.fabrick.dto;

import java.util.List;

public class OutputTransazioniDto {

	private List<OutputTransazioneDto> list;

	public List<OutputTransazioneDto> getList() {
		return list;
	}

	public void setList(List<OutputTransazioneDto> list) {
		this.list = list;
	}
	
}
