package it.marco.fabrick;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.DependsOn;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class FabrickApplication {

	public static void main(String[] args) {
		SpringApplication.run(FabrickApplication.class, args);
	}
	
	@Bean
	@DependsOn(value = {"customRestTemplateCustomizer"})
	public RestTemplateBuilder restTemplateBuilder() {
	    return new RestTemplateBuilder(customRestTemplateCustomizer());
	}
	
	@Bean
	public RestTemplate restTemplate(RestTemplateBuilder builder) {
		return builder.build();
	}
	
	@Bean
	public CustomRestTemplateCustomizer customRestTemplateCustomizer() {
	    return new CustomRestTemplateCustomizer();
	}

}
