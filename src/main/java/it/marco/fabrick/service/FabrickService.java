package it.marco.fabrick.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import it.marco.fabrick.dto.InputBonificoDto;
import it.marco.fabrick.dto.InputTransazioniDto;
import it.marco.fabrick.entity.TransazioniEntity;
import it.marco.fabrick.json.BonificoJson;
import it.marco.fabrick.json.DettaglioBonificoJson;
import it.marco.fabrick.json.DettaglioSaldoJson;
import it.marco.fabrick.json.DettaglioTransazioneJson;
import it.marco.fabrick.json.DettaglioTransazioniJson;
import it.marco.fabrick.json.SaldoJson;
import it.marco.fabrick.json.TransazioniJson;
import it.marco.fabrick.repository.TransazioniRepository;
import it.marco.fabrick.utils.Constants;

@Service("fabrickService")
public class FabrickService {
	
	private static final Logger logger = LoggerFactory.getLogger(FabrickService.class);
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private TransazioniRepository transazioniRepository;

	public DettaglioSaldoJson letturaSaldoOperation (Long accountId) {

		logger.info("letturaSaldoOperation");
		String url = Constants.BASE_URL + Constants.LETTURA_SALDO.replace("{accountId}", String.valueOf(accountId));
		SaldoJson output = restTemplate.getForObject(url, SaldoJson.class);
		
		return output.getPayload();
	}
	
	public DettaglioBonificoJson bonificoOperation (InputBonificoDto input) {
		
		logger.info("bonificoOperation");
		String url = Constants.BASE_URL + Constants.BONIFICO.replace("{accountId}", String.valueOf(input.getAccountId()));
		ResponseEntity<BonificoJson> output = restTemplate.postForEntity(url, null, BonificoJson.class);
		
		return output.getBody().getPayload();
	}
	
	public DettaglioTransazioniJson letturaTransazioniOperation (InputTransazioniDto input) {
		
		logger.info("letturaTransazioniOperation");
		String url = Constants.BASE_URL + Constants.LETTURA_TRANSAZIONI;
		TransazioniJson output = restTemplate.getForObject(url, TransazioniJson.class);
		
		output.getPayload().getList().forEach(t -> {
			TransazioniEntity entity = fillEntity(t);
			transazioniRepository.save(entity);
		});
		
		return output.getPayload();
	}
	
	private TransazioniEntity fillEntity(DettaglioTransazioneJson dett) {
		TransazioniEntity entity = new TransazioniEntity();
		//qui si dovrebbero settare il resto dei dati da voler salvare sul db
		entity.setAbiCode(dett.getAbiCode());
		return entity;
	}
	
}
