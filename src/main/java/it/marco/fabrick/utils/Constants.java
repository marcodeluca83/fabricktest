package it.marco.fabrick.utils;

public class Constants {

	public static final String BASE_URL = "https://sandbox.platfr.io";
	
	public static String LETTURA_SALDO = "/api/gbs/banking/v4.0/accounts/{accountId}/balance";
	public static String BONIFICO = "/api/gbs/banking/v4.0/accounts/{accountId}/payments/money-transfers";
	public static final String LETTURA_TRANSAZIONI = "/api/gbs/banking/v4.0/accounts";
	
	public static final Long ACCOUNT_ID = 14537780L;
	//public static final Long idChiave = 3202L;
    
    public static final String AUTH_SCHEMA = "Auth-Schema";
    public static final String S2S = "S2S";
    public static final String API_KEY = "Api-Key";
    public static final String KEY = "FXOVVXXHVCPVPBZXIJOBGUGSKHDNFRRQJP";
    
}
